import React from 'react'

const Button = ({ backgroundColor, text, onClick }) => {
  const buttonStyle = { backgroundColor: backgroundColor };
  return (
    <button style={buttonStyle} onClick={onClick}>
      {text}
    </button>
  );
};
export default Button;