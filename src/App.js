import {useState} from "react";
import Button from './components/Button.js';
import Modal from './components/Modal.js';
import './styles.scss'; 

function App() {
  const [isFirstOpen, setFirstModal] = useState(false);
  const [isSecondOpen, setSecondModal] = useState(false);

  const openFirst = () => {
    setFirstModal(true);
  };

  const closeFirst = () => {
    setFirstModal(false);
  };

  const openSecond = () => {
    setSecondModal(true);
  };

  const closeSecond = () => {
    setSecondModal(false);
  };

  const firstActions = (
    <div>
      <button className="modalBtn" onClick={closeFirst}>Ok</button>
      <button className="modalBtn" onClick={closeFirst}>Cancel</button>
    </div>
  );

  const secondActions = (
    <div>
      <button onClick={closeSecond}>Send</button>
      <button onClick={closeSecond}>Do something</button>
    </div>
  );


  return (
    <div>
    <Button backgroundColor="blue" text="Open first modal" onClick={openFirst} />
    <Button backgroundColor="green" text="Open second modal" onClick={openSecond} />
    <Modal
        header="Do you want to delete this file?"
        closeButton={true}
        text="Once you delete this file, it won't be possible to undo this action.
        Are you sure you want to delete it?"
        actions={firstActions}
        isOpen={isFirstOpen}
        onClose={closeFirst}
      />
    <Modal
        header="Second modal header"
        closeButton={true}
        text="Main text of second modal."
        actions={secondActions}
        isOpen={isSecondOpen}
        onClose={closeSecond}
      />
    </div>
  );
}

export default App;
